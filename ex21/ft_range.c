/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmonneri <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/07 11:24:15 by jmonneri          #+#    #+#             */
/*   Updated: 2017/11/07 18:28:03 by jmonneri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>

int		*ft_range(int min, int max)
{
	int *dest;
	int i;

	i = 0;
	if (min >= max || !(dest = (int*)malloc(sizeof(dest) * (max - min + 1))))
		return (NULL);
	while (min < max)
		dest[i++] = min++;
	return (dest);
}
