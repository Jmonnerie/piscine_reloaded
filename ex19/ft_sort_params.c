/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_params.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmonneri <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/06 21:34:53 by jmonneri          #+#    #+#             */
/*   Updated: 2017/11/07 17:37:23 by jmonneri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

int		ft_strcmp(char *s1, char *s2)
{
	int i;

	i = 0;
	while (s1[i] == s2[i] && s1[i])
		i++;
	return (s1[i] - s2[i]);
}

void	ft_print_params(char **argv)
{
	int i;
	int j;

	i = 1;
	while (argv[i])
	{
		j = 0;
		while (argv[i][j])
			ft_putchar(argv[i][j++]);
		ft_putchar('\n');
		i++;
	}
	return ;
}

int		main(int argc, char **argv)
{
	int		i;
	int		j;
	void	*swap;

	i = 0;
	if (argc >= 3)
	{
		while (argv[i])
		{
			j = 0;
			while (argv[j])
			{
				if (ft_strcmp(argv[i], argv[j]) < 0)
				{
					swap = argv[i];
					argv[i] = argv[j];
					argv[j] = swap;
				}
				j++;
			}
			i++;
		}
	}
	ft_print_params(argv);
	return (0);
}
