/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmonneri <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/07 16:53:52 by jmonneri          #+#    #+#             */
/*   Updated: 2017/11/07 18:53:51 by jmonneri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_header.h"

void	ft_putbuf(char *str)
{
	int i;

	i = 0;
	while (str[i])
		write(1, &str[i++], 1);
	return ;
}

void	ft_display_error(int i)
{
	if (i == 2)
		ft_putbuf("File name missing.\n");
	if (i == 1)
		ft_putbuf("Too many arguments.\n");
	return ;
}

int		main(int argc, char **argv)
{
	int		fd;
	int		ret;
	char	buf[BUF_SIZE + 1];

	if (argc < 2)
		ft_display_error(2);
	else if (argc > 2)
		ft_display_error(1);
	else
	{
		fd = open(argv[1], O_RDONLY);
		while ((ret = read(fd, buf, BUF_SIZE)))
		{
			buf[ret] = '\0';
			ft_putbuf(buf);
		}
		close(fd);
	}
	return (0);
}
